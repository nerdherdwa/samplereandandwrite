﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace SampleWriteNReadToFiles
{
    class Program
    {
        static void Main(string[] args)
        {
            string line;
            try
            {
                StreamReader sr = new StreamReader(Constants.filePathName);
                line = sr.ReadLine();

                while(line!= null)
                {
                    Console.WriteLine(line);
                    line = sr.ReadLine();
                }
                sr.Close();

                Console.WriteLine("press a key to write to file");
                Console.ReadKey();

                StreamWriter sw = new StreamWriter(Constants.filePathName, append: true);
                sw.WriteLine();
                sw.WriteLine("added new lines to file");
                sw.WriteLine("VHS hexagon tumeric yuccie.");
                sw.WriteLine("Viral tattooed salvia irony brunch venmo sriracha banh mi fam narwhal squid subway tile tacos.");
                sw.Close();


            } catch(Exception ex)
            {
                Console.WriteLine("Exception: " + ex.Message);
            }
            finally
            {
                Console.WriteLine("Executing finally block.");

            }
            Console.Write("press any key to exit.");
            Console.ReadKey();

        }
    }
}
